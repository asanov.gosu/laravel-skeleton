#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER laravel;
    CREATE DATABASE laravel;
    GRANT ALL PRIVILEGES ON DATABASE laravel TO laravel;
EOSQL
